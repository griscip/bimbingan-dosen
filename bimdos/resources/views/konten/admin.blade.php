@extends('layout.app')
@section('content')

<h1 class="display-4 text-center my-5" id="judul">
  Data Dosen
  <a type="button" class="btn btn-outline-primary" href="{{url('/create/dosen')}}">Tambah Dosen</a>
</h1>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>NIP</th>
      <th>Nama Dosen</th>
      <th>id Dosen</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($Dosen as $dos)
    <tr>
      <td>{{$loop->iteration}}</td>
      <td>{{$dos->nip}}</td>
      <td>{{$dos->nama}}</td>
      <td>{{$dos->id_dosbing}}</td>
      <td>
        <form action="/dosen/{{$dos->id}}" method="POST" class="d-inline">
          <div class="d-grid gap-2 d-md-1 justify-content-md-end">
            <a class="btn bbtn btn-outline-info" href="/dosen/{{$dos->id}}/edit">Edit</a>
            @method('delete')
            @csrf
            <button class="btn btn btn-outline-danger" type="submit">Delete</button>
          </div>
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

<br>
<hr size="10px" color="grey">

<h1 class="display-4 text-center my-5" id="judul">
  Data Mahasiswa
  <a type="button" class="btn btn-outline-primary" href="{{url('/create/mahasiswa')}}">Tambah Mahasiswa</a>
</h1>

<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>NIM</th>
      <th>Nama Mahasiswa</th>
      <th>Jurusan</th>
      <th>Dosen Pembimbing</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($Mahasiswa as $mhs)
    <tr>
      <td>{{$loop->iteration}}</td>
      <td>{{$mhs->nim}}</td>
      <td>{{$mhs->nama}}</td>
      <td>{{$mhs->jurusan}}</td>
      <td>{{$mhs->relasi->nama}}</td>
      <td>
        <form action="/mahasiswa/{{$mhs->id}}" method="POST" class="d-inline">
          <div class="d-grid gap-2 d-md-1 justify-content-md-end">
            <a class="btn bbtn btn-outline-info" href="/mahasiswa/{{$mhs->id}}/edit">Edit</a>
            @method('delete')
            @csrf
            <button class="btn btn btn-outline-danger" type="submit">Delete</button>
          </div>
        </form>

      </td>
    </tr>
    @endforeach
  </tbody>
</table>

@endsection