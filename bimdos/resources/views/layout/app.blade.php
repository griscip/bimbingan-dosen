<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Sistem Bimbingan Dosen</title>
  <link rel="icon" href="img/favicon.png" type="image/png">
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>

  <!-- NAVBAR -->
  <nav id="main-navbar" class="navbar navbar-expand-lg navbar-light
bg-white py-3">
    <div class="container pb-3">
      <a class="navbar-brand" href="">
        <span class="d-none">Bimdos</span>
        <img src="/img/logo.png" class="main-logo d-none d-xl-inline" alt="bimdos logo">
        <img src="/img/logo.png" class="small-logo d-inline d-xl-none" alt="bimdos logo">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav m-auto">
          <!-- <li class="nav-item">
            <a class="nav-link px-4" href="">
              Dosen</a>
          </li>
          <li class="nav-item">
            <a class="nav-link px-4" href="">
              Mahasiswa</a>
          </li> -->
        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
          <!-- Authentication Links -->
          @guest
          @if (Route::has('login'))
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @endif

          @if (Route::has('register'))
          <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
          </li>
          @endif
          @else
          <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->name }}
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
              </form>
            </div>
          </li>
          @endguest
        </ul>
      </div>
    </div>
  </nav>



  <div class="container mt-3" style="min-height:550px">
    <div class="row">
      <div class="col-12">

        @yield('content')

      </div>
    </div>
  </div>



  <!-- FOOTER -->
  <footer id="main-footer" class="text-white bg-dark py-4 mt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-3 text-center text-md-left">
          <a href="{{ url('/') }}">
            <img src="/img/logo_besar.png" style="max-height: 60px;" class="mb-3 img-fluid">
          </a>
          <p>
            BIMDOS adalah Sistem Bimbingan Dosen untuk memudahkan Mahasiswa dan Dosen dalam proses Bimbingan
          </p>
        </div>
        <div class="col-md-3 text-center d-none d-md-inline">
          <h5>Information</h5>

        </div>
        <div class="col-md-3 text-center">
          <h5>Our Services</h5>
          <ul class="list-unstyled">
            <!-- @guest
          @if (Route::has('register')) -->
            <li>
              <a class="text-white" href="">
                <!-- {{ __('Register') }} -->
              </a>
            </li>
            <!-- @endif
          @endguest -->
            <li><a href="#" class="text-white">Help/Contact Us</a></li>
            <li><a href="#" class="text-white">Privacy Policy</a></li>
            <li><a href="#" class="text-white">Terms & Conditions</a></li>
          </ul>
        </div>
        <div class="col-md-3 text-center text-md-left">
          <h5>Hubungi Kami</h5>
          <div class="text-nowrap"><i class="fas fa-envelope fa-fw mr-3"></i>
            info@bimdos.ac.id</div>
          <div class="text-nowrap"><i class="fas fa-phone fa-fw mr-3"></i>
            (021) 123456</div>
          <div class="text-nowrap"><i class="fas fa-globe fa-fw mr-3"></i>
            www.Bimdos.ac.id</div>
        </div>
      </div>
      <div class="row mt-3 mt-md-0">
        <div class="col-md-3 mr-md-auto text-center text-md-left">
          <small>&copy; BIMDOS {{date("Y")}}</small>
        </div>
        <div id="footer-icon" class="col-md-3 text-center text-md-left">
          <div>
            <a href="#" class="text-white mr-1">
              <i class="fab fa-facebook fa-lg"></i>
            </a>
            <a href="#" class="text-white mr-1">
              <i class="fab fa-twitter fa-lg"></i>
            </a>
            <a href="#" class="text-white mr-1">
              <i class="fab fa-instagram fa-lg"></i>
            </a>
            <a href="#" class="text-white mr-1">
              <i class="fab fa-google-plus fa-lg"></i>
            </a>
            <a href="#" class="text-white mr-1">
              <i class="fab fa-github fa-lg"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </footer>
</body>

</html>