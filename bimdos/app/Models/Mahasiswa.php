<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    // use HasFactory;
    protected $fillable = ['nim','nama','id_dosbing','jurusan'];
    public function relasi(){
        return $this->belongsTo(Dosen::class, 'id_dosbing', 'id_dosbing');
    }
}
