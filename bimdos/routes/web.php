<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\MahasiswasController;
use App\Http\Controllers\DosensController;
use App\Http\Controllers\PagesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('konten.dosen');
// });

Route::get('/mahasiswa', 'PagesController@mhasiswa');
Route::view('/mahasiswa', 'konten.mahasiswa');
Route::view('/dosen', 'konten.dosen');
Route::view('/home', 'konten.home');

// Route::get('/', [PagesController::class, 'home']);
// Route::get('/admin', [PagesController::class, 'admin']);
// Route::get('/mahasiswa', [PagesController::class, 'mahasiswa']);
// Route::get('/dosen', [PagesController::class, 'dosen']);

Route::get('/', [AdminController::class, 'index']);
Route::get('/create/mahasiswa', [MahasiswasController::class, 'create']);
Route::get('/create/dosen', [DosensController::class, 'create']);
Route::post('/mahasiswa', [MahasiswasController::class, 'store']);
Route::post('/dosen', [DosensController::class, 'store']);
Route::delete('/mahasiswa/{mahasiswa}',[MahasiswasController::class, 'destroy']);
Route::delete('/dosen/{dosen}',[DosensController::class, 'destroy']);
Route::get('/mahasiswa/{mahasiswa}/edit',[MahasiswasController::class, 'edit']);
Route::get('/dosen/{dosen}/edit',[DosensController::class, 'edit']);
Route::patch('/mahasiswa/{mahasiswa}',[MahasiswasController::class, 'update']);
Route::patch('/dosen/{dosen}',[MahasiswasController::class, 'update']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
